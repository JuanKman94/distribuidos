#include <pthread.h>
#include <stdlib.h>
#include <stdio.h>
#include <errno.h>
#include <time.h>

struct fac_nums {
	int high;
	int low;
};

void *fac_t(void *ptr);
long double fac(int high, int low);

int main(int argc, char *argv[])
{
	double n,	// number of threads
		my_num;
	int i,	// counter
		z,	// status
		chunk;
	char *endptr = NULL;
	struct fac_nums* args_nums,
		nums;

	pthread_t *thds;
	clock_t cl_begin,
			cl_end;

	if ( argc < 3 ) {
		fprintf(stderr, "I need 2 arguments: <num_threads> <num>\n");
		exit(EXIT_FAILURE);
	}

	// parse numbers from arguments
	n = strtod(argv[1], &endptr);
	my_num = strtod(argv[2], &endptr);

	if (errno != 0 || n < 1 || my_num < 1) {
		perror("strtod");
		exit(EXIT_FAILURE);
	}

	thds = calloc(sizeof(pthread_t), n);
	if (thds == NULL) {
		perror("calloc(threads)");
		exit(EXIT_FAILURE);
	}
	args_nums = calloc(sizeof(struct fac_nums), n);
	if (args_nums == NULL) {
		perror("calloc(args_nums)");
		exit(EXIT_FAILURE);
	}

	// split my_num in calculation ranges
	chunk = my_num / n;

	cl_begin = clock();


	// start threads
	for (i = 0; i < n; i++) {
		nums.high = chunk * (i+1);
		nums.low = chunk * i;

		args_nums[i] = nums;

		z = pthread_create(&thds[i], NULL, fac_t, (void*)&args_nums[i]);

		if (z != 0) {
			fprintf(stderr, "Error creating thread %d. return code: %d\n",
						i,
						z
					);
		}
	}

	// wait for all threads
	for (i = 0; i < n; i++) {
		z = pthread_join(thds[i], NULL);

		if (z != 0)
			perror("pthread_join");
	}

	cl_end = clock();

	printf("%.0f %f\n", n, (double)(cl_end - cl_begin) / CLOCKS_PER_SEC);

	free(thds);
	free(args_nums);

	exit(EXIT_SUCCESS);
}

void *fac_t(void *ptr)
{
	struct fac_nums *nums = NULL;
	long double f;

	nums = (struct fac_nums*)ptr;
	f = fac(nums->high, nums->low);

	return NULL;
}

long double fac(int high, int low)
{
	if (high > 1 && high > low) {
		return high * fac(high - 1, low);
	}

	return high;
}
