#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <pthread.h>

#define FAC_NUM 5

void *fac_t(void *ptr);
long double fac(long double n);

int main(int argc, char *argv[])
{
	int i,	// counter
		z;	// status
	long double nums[FAC_NUM];
	char *endptr = NULL;

	// pthread stuff
	pthread_t t[FAC_NUM];
	void *resp;

	// get numbers
	for (i = 1; i <= FAC_NUM; i++) {
		if (i < argc) {
			// parse numbers from arguments
			nums[i-1] = strtold(argv[i], &endptr);
			printf("nums[%d] = %Lf\n", i-1, nums[i-1]);

			if (errno != 0) {
				perror("strtold");
			}
		} else {
			// ask the user for the numbers
			z = EOF;
			while (z == EOF || nums[i-1] == 0) {
				errno = 0;
				printf("Write number %d: ", i);
				z = scanf("%Lf", &nums[i-1]);
				fflush(stdin);

				if (errno != 0) {
					perror("scanf");
				}
			}
		}
	}

	// initialize threads
	for (i = 0; i < FAC_NUM; i++) {
		z = pthread_create(&t[i], NULL, fac_t, (void*)&nums[i]);

		if (z != 0) {
			fprintf(stderr, "Error creating thread %d. return code: %d\n",
						i,
						z
					);
		}
	}

	for (i = 0; i < FAC_NUM; i++) {
		z = pthread_join(t[i], &resp);

		if (z != 0) {
			perror("pthread_join()");
		} else if (resp != NULL) {
			printf("factorial(%.0Lf): %.0Lf\n", nums[i], *(long double*)resp);
			free(resp);
		}
	}

	exit(EXIT_SUCCESS);
}

void *fac_t(void *ptr)
{
	long double *n,
		 *f = NULL;

	n = (long double*)ptr;
	f = malloc(sizeof(long double));

	if (f == NULL) {
		perror("malloc()");
	} else {
		*f = fac(*n);
	}

	return f;
}

long double fac(long double n)
{
	if (n > 1) {
		return n * fac(n - 1);
	}

	return n;
}
