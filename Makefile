# compiler
CC = gcc

# compiler flags
# 	-g			debug info to executable object
# 	-Wall		most compiler's warnings enabled
# 	-pthread	link pthread library
CFLAGS = -g -Wall -pthread

ARGS_NUMS = 1754 1400 1500 1600 1700

.PHONY: clean

build-assignment1: clean assignment1/main.c
	$(CC) $(CFLAGS) assignment1/main.c

build-assignment2: clean assignment2/main.c
	$(CC) $(CFLAGS) assignment2/main.c thpool/thpool.c -D THPOOL_DEBUG

build-assignment3: clean assignment3/main.c
	$(CC) $(CFLAGS) assignment3/main.c

assignment1: build-assignment1
	./a.out $(ARGS_NUMS)

assignment2: build-assignment2
	./a.out $(ARGS_NUMS)

assignment3: build-assignment3
	for i in $$(seq 1 32); do ./a.out $$i 1754; done > results.dat

clean:
	$(RM) a.out results.dat
