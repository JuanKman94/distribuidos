# Distributed and Parallel Systems

## Assignment 1
Write a program that prints 5 factorial numbers simultaneously

## Assignment 2
Implement a pool thread in assignment 1. Also get each thread's execution time
either in seconds or clock cycles

## Assignment 3
Split the factorial calculation in `n` operations, where `n` is the number
of threads used. Plot the execution time vs. the number of threads used.
To plot with `gnuplot` do the following

```bash
$ make assignment3
$ gnuplot -persistent assignment3/plot
```
*NOTE*: factorial number beyond 1754 results in `inf`.
