#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <pthread.h>
#include <time.h>
#include "../thpool/thpool.h"

#define NTHREADS 5

void fac_t(void *ptr);
long double fac(long double n);

clock_t cl_begin;

int main(int argc, char *argv[])
{
	int i,	// counter
		z;	// status
	long double nums[NTHREADS];
	char *endptr = NULL;

	// define threadpool
	threadpool thpool = thpool_init(NTHREADS);

	// get numbers
	for (i = 1; i <= NTHREADS; i++) {
		if (i < argc) {
			// parse numbers from arguments
			nums[i-1] = strtold(argv[i], &endptr);
			printf("nums[%d] = %Lf\n", i-1, nums[i-1]);

			if (errno != 0) {
				perror("strtold");
			}
		} else {
			// ask the user for the numbers
			z = EOF;
			while (z == EOF || nums[i-1] == 0) {
				errno = 0;
				printf("Write number %d: ", i);
				z = scanf("%Lf", &nums[i-1]);
				fflush(stdin);

				if (errno != 0) {
					perror("scanf");
				}
			}
		}
	}

	cl_begin = clock();

	// initialize threads
	for (i = 0; i < NTHREADS; i++) {
		// add task to threadpool
		thpool_add_work(thpool, (void*)fac_t, (void*)&nums[i]);
	}

	// destroy threadpool
	thpool_wait(thpool);
	thpool_destroy(thpool);

	exit(EXIT_SUCCESS);
}

void fac_t(void *ptr)
{
	long double *n,
		 f = 0;
	clock_t cl_end;

	n = (long double*)ptr;
	f = fac(*n);
	cl_end = clock();

	printf("factorial(%.0Lf): %.0Lf; "
			"clock time: %f secs; %f cycles\n",
				*n,
				f,
				(double)(cl_end - cl_begin) / CLOCKS_PER_SEC,
				(double)(cl_end - cl_begin)
			);
}

long double fac(long double n)
{
	if (n > 1) {
		return n * fac(n - 1);
	}

	return n;
}
